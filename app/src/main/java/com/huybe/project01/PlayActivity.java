package com.huybe.project01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener {
    private final String[] ANSWER = {
            "AOMUA",
            "BAOCAO",
            "CANTHIEP",
            "CATTUONG",
            "CHIEUTRE",
            "DANHLUA",
            "DANONG",
            "GIANDIEP",
            "GIANGMAI",
            "HOIDONG",
            "HONGTAM",
            "KHOAILANG",
            "KIEMCHUYEN",
            "LANCAN",
            "MASAT",
            "NAMBANCAU",
            "OTO",
            "QUYHANG",
            "SONGSONG",
            "THATTINH",
            "THOTHE",
            "TICHPHAN",
            "TOHOAI",
            "TOTIEN",
            "TRANHTHU",
            "VUAPHALUOI",
            "VUONBACHTHU",
            "XAKEP",
            "XAPHONG",
            "XEDAPDIEN"

    };
    private static final int[] IMG_QUESTIONS = {
            R.drawable.aomua,
            R.drawable.baocao,
            R.drawable.canthiep,
            R.drawable.cattuong,
            R.drawable.chieutre,
            R.drawable.danhlua,
            R.drawable.danong,
            R.drawable.giandiep,
            R.drawable.giangmai,
            R.drawable.hoidong,
            R.drawable.hongtam,
            R.drawable.khoailang,
            R.drawable.kiemchuyen,
            R.drawable.lancan,
            R.drawable.masat,
            R.drawable.nambancau,
            R.drawable.oto,
            R.drawable.quyhang,
            R.drawable.songsong,
            R.drawable.thattinh,
            R.drawable.thothe,
            R.drawable.tichphan,
            R.drawable.tohoai,
            R.drawable.totien,
            R.drawable.tranhthu,
            R.drawable.vuaphaluoi,
            R.drawable.vuonbachthu,
            R.drawable.xakep,
            R.drawable.xaphong,
            R.drawable.xedapdien,
    };

    private static final int[] BUTTON_IDS = {
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    };

    private Button btnNextGame;
    private TextView txt_heart;
    private TextView txtResult;
    private TextView txt_coin;
    private FrameLayout layoutImage;
    private LinearLayout layoutButtonAnswer1;
    private LinearLayout layoutButtonAnswer2;
    private LinearLayout layoutButtonRecommend1;
    private LinearLayout layoutButtonRecommend2;
    private List<Button> btnAnswer;
    private List<Button> btnRecommend;

    private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private Random random;
    private List<Integer> listImgQuestionOld = new ArrayList<>();
    private String result = "";
    private int rd = 0;
    private int i = 0;
    private int coin = 0;
    private int heart = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        init();
        initGame();
    }

    private void init() {
        btnNextGame = (Button) findViewById(R.id.btn_next);
        txtResult = (TextView) findViewById(R.id.txt_result);
        txt_coin = (TextView) findViewById(R.id.txt_coin);
        txt_heart = (TextView) findViewById(R.id.txt_heart);
    }

    private void regtisgerListener() {
        btnNextGame.setOnClickListener(this);
    }

    private void createImage() {
        layoutImage = (FrameLayout) findViewById(R.id.layout_image);
        layoutImage.removeAllViews();
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(IMG_QUESTIONS[rd]);
        ImageView imageView1 = new ImageView(this);
        imageView1.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView1.setImageResource(R.drawable.bg_picture_border);
        layoutImage.addView(imageView);
        layoutImage.addView(imageView1);
    }

    //tao button xam
    private void createButtonAnswer() {
        layoutButtonAnswer1 = (LinearLayout) findViewById(R.id.layout_btn_answer1);
        layoutButtonAnswer2 = (LinearLayout) findViewById(R.id.layout_btn_answer2);
        layoutButtonAnswer1.removeAllViews();
        layoutButtonAnswer2.removeAllViews();

        //so luong button bang tong ky tu cua cau tra loi
        btnAnswer = new ArrayList<>();
        for (int i = 0; i < ANSWER[rd].length(); i++) {
            Button button = new Button(this);
            button.setLayoutParams(new LinearLayout.LayoutParams(120, 150));
            button.setBackgroundResource(R.drawable.ic_button_xam);
            button.setId(i);
            btnAnswer.add(button);
            if (i < 8) {
                layoutButtonAnswer1.addView(button);
            } else {
                layoutButtonAnswer2.addView(button);
            }
        }
    }

    //tao button vang
    private void createButtonRecommend() {
        layoutButtonRecommend1 = (LinearLayout) findViewById(R.id.layout_recommend1);
        layoutButtonRecommend2 = (LinearLayout) findViewById(R.id.layout_recommend2);
        layoutButtonRecommend1.removeAllViews();
        layoutButtonRecommend2.removeAllViews();

        // tao list chua 16 ky tu goi y =  cau tra loi + random ky tu trong bang chu cai
        List<String> listAnswer = new ArrayList<>();
        for (int j = 0; j < ANSWER[rd].length(); j++) {
            String str = String.valueOf(ANSWER[rd].charAt(j));
            listAnswer.add(str);
        }
        int a = 16 - listAnswer.size();
        for (int i = 0; i < a; i++) {
            listAnswer.add(String.valueOf(alphabet.charAt(newRandom(25))));
        }
        System.out.println(listAnswer.toString());

        //tao list button vang so luong bang 16
        btnRecommend = new ArrayList<>();
        for (int j = 0; j < 16; j++) {
            final Button button = new Button(this);
            button.setLayoutParams(new LinearLayout.LayoutParams(120, 160));
            button.setBackgroundResource(R.drawable.ic_tile_hover);
            int random = newRandom(listAnswer.size());
            button.setText(listAnswer.get(random));//random ky tu vao button
            listAnswer.remove(random);//xoa ky tu da dung trong list
            button.setId(BUTTON_IDS[j]);
            btnRecommend.add(button);
            if (j % 2 == 0) {
                layoutButtonRecommend1.addView(button);
            } else {
                layoutButtonRecommend2.addView(button);
            }
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (i < ANSWER[rd].length()) {
                        btnAnswer.get(i).setText(button.getText());
                        i++;
                        result += button.getText();
                        button.setVisibility(View.INVISIBLE);
                    }
                    //xu ly kiem tra dap an
                    if (result.length() == ANSWER[rd].length()) {
                        for (int i = 0; i < btnRecommend.size(); i++) {
                            btnRecommend.get(i).setEnabled(false);
                        }
                        if (result.equals(ANSWER[rd])) {
                            txtResult.setVisibility(View.VISIBLE);
                            txtResult.setText("Chính xác!");
                            coin += 100;
                            txt_coin.setText(String.valueOf(coin));
                            btnNextGame.setVisibility(View.VISIBLE);
                            for (int i = 0; i < ANSWER[rd].length(); i++) {
                                btnAnswer.get(i).setBackgroundResource(R.drawable.ic_tile_true);

                            }
                        } else {
                            txtResult.setVisibility(View.VISIBLE);
                            txtResult.setText("Sai rồi!");
                            heart -= 1;
                            txt_heart.setText(String.valueOf(heart));
                            btnNextGame.setVisibility(View.VISIBLE);
                            for (int i = 0; i < ANSWER[rd].length(); i++) {
                                btnAnswer.get(i).setBackgroundResource(R.drawable.ic_tile_false);

                            }
                        }
                    }
                    //xu ly game over va win game
                    if (heart == 0) {
                        Toast.makeText(getApplication(), "Game Over", Toast.LENGTH_LONG).show();
                        finish();
                    }
                    if (listImgQuestionOld.size() == 30) {
                        Toast.makeText(getApplication(), "Bạn đã chiến thắng!", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }
    }

    //random cau hoi
    private int randomQuestion() {
        random = new Random();
        while (listImgQuestionOld.contains(rd) == true) {
            rd = random.nextInt(IMG_QUESTIONS.length);
        }
        return rd;
    }

    private int newRandom(int n) {
        random = new Random();

        return random.nextInt(n);
    }

    @Override
    public void onClick(View view) {
        //xu ly onClick cho button nextGame
        switch (view.getId()) {
            case R.id.btn_next:
                initGame();
                break;
            default:
                break;
        }
    }

    private void initGame() {
        result = "";
        btnNextGame.setVisibility(View.INVISIBLE);
        txtResult.setVisibility(View.INVISIBLE);
        i = 0;
        rd = randomQuestion();
        listImgQuestionOld.add(rd);
        regtisgerListener();
        createImage();
        createButtonRecommend();
        createButtonAnswer();
    }
}
